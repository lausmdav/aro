#!/usr/bin/env python3
from __future__ import absolute_import, division, print_function

# Setup python paths first.
import os
import sys
import rospy
from nav_msgs.msg import OccupancyGrid
from geometry_msgs.msg import TransformStamped
import subprocess
from threading import Timer
import tf.transformations as tft
import tf2_ros
from aro_msgs.srv import GenerateFrontier
from timeit import default_timer as c_timer

dir = os.path.dirname(os.path.abspath(__file__))

print('Python version: %s' % sys.version)
print('Python version info: %s' % (sys.version_info,))

import base64
from glob import glob
import mimetypes
import numpy as np
from shutil import copytree

#np.set_printoptions(suppress=True)

class FrontierTester():
    def __init__(self):
        self.gridSubscriber = rospy.Subscriber('occupancy', OccupancyGrid, self.grid_cb)
        self.tf_pub = tf2_ros.TransformBroadcaster()
        self.x = 0
        self.y = 0

    def shutdown(self):
        self.gridSubscriber.unregister()

    def grid_cb(self, msg):
        self.publish_position()


    def publish_position(self):
        tf_msg = TransformStamped()
        tf_msg.header.stamp = rospy.Time.now()
        tf_msg.transform.translation.x = self.x
        tf_msg.transform.translation.y = self.y
        tf_msg.transform.translation.z = 0
        q = tft.quaternion_from_euler(0, 0, 0)
        tf_msg.transform.rotation.x = q[0]
        tf_msg.transform.rotation.y = q[1]
        tf_msg.transform.rotation.z = q[2]
        tf_msg.transform.rotation.w = q[3]
        tf_msg.header.frame_id = 'icp_map'
        tf_msg.child_frame_id = 'base_footprint'
        self.tf_pub.sendTransform(tf_msg)

        tf_msg = TransformStamped()
        tf_msg.header.stamp = rospy.Time.now()
        tf_msg.header.frame_id = 'map'
        tf_msg.child_frame_id = 'icp_map'
        tf_msg.transform.rotation.w = 1
        self.tf_pub.sendTransform(tf_msg)

    def set_pos(self, x, y):
        self.x = x
        self.y = y


if __name__ == '__main__':
    total = 0.0
    messages = ''
    max_dist = 0.3 # Acceptable imprecision.
    output = []

    if not os.path.isfile(os.path.join(dir,'frontier.py')):
        rospy.loginfo('Frontier script not found.')
        pass
    else:
        roscore_proc = subprocess.Popen(['roscore'])
        rospy.init_node("tester")
        # rospy.set_param('robot_diameter', 0.8)
        # rospy.set_param('occupancy_treshold', 90)
        # rospy.set_param('map_frame', 'icp_map')
        # rospy.set_param('robot_frame', 'base_footprint')
        player_proc = subprocess.Popen(['rosbag', 'play', '../../data/planning/map_dat.bag'], cwd=dir)

        kill = lambda process : process.terminate()

        script_output_file = 'output.txt'
        with open(script_output_file, 'w') as f_output:
            frontier_proc = subprocess.Popen(['rosrun', 'aro_exploration', 'frontier.py'], stdout=f_output, stderr=f_output)

        rospy.sleep(5)
        timeout = 30
        scan_timer = Timer(timeout, kill, [frontier_proc])
        scan_timer2 = Timer(timeout, kill, [roscore_proc])
        scan_timer3 = Timer(timeout, kill, [player_proc])
        serv_start = False

        frontier = FrontierTester()
        frontier.set_pos(0,0)

        try:
            scan_timer.start()
            scan_timer2.start()
            scan_timer3.start()
            t = c_timer()
            try:
                rospy.wait_for_service('get_closest_frontier', 10)
                get_frontier = rospy.ServiceProxy('get_closest_frontier', GenerateFrontier)
                serv_start = True
            except Exception as e: 
                rospy.loginfo('Exception caught') 
                
            if serv_start: 

                frontier.set_pos(-0.3,0.7)
                rospy.sleep(1)
                rospy.loginfo('Test 1:')
                try:
                    coords = get_frontier()
                    if coords and coords.goal_pose:
                        x = coords.goal_pose.x
                        y = coords.goal_pose.y
                        if np.sqrt((-0.8-x)**2 + (1.0-y)**2) > max_dist:
                            rospy.loginfo('Frontier pose [' + str(x) + ', '+str(y)+'] is too far from closest frontier.')
                        else:
                            rospy.loginfo('Frontier pose [' + str(x) + ', '+str(y)+'] is close enough.')

                except Exception as e:
                    rospy.loginfo('[EVALUATION] Exception caught: %s.', e)
                    if (c_timer() - t) < timeout:
                        rospy.loginfo('Exception caught during getting frontier. Check script\'s output.')
                    else:
                        rospy.loginfo('Internal timeout exceeded. Get closest frontier service response time is too high.')


                frontier.set_pos(-0.3,-1.0)
                rospy.sleep(1)
                rospy.loginfo('Test 2:')
                try:
                    coords = get_frontier()
                    if coords and coords.goal_pose:
                        x = coords.goal_pose.x
                        y = coords.goal_pose.y
                        if np.sqrt((-1.05-x)**2 + (-0.9-y)**2) > max_dist:
                            rospy.loginfo('Frontier pose [' + str(x) + ', '+str(y)+'] is too far from closest frontier.')
                        else:
                            rospy.loginfo('Frontier pose [' + str(x) + ', '+str(y)+'] is close enough.')

                except Exception as e:
                    rospy.loginfo('[EVALUATION] Exception caught: %s.', e)
                    if (c_timer() - t) < timeout:
                        rospy.loginfo('Exception caught during getting frontier. Check script\'s output.')
                    else:
                        rospy.loginfo('Internal timeout exceeded. Get closest frontier service response time is too high.')


                frontier.set_pos(-0.2,-1.7)
                rospy.sleep(1)
                rospy.loginfo('Test 3:')
                try:
                    coords = get_frontier()
                    if coords and coords.goal_pose:
                        x = coords.goal_pose.x
                        y = coords.goal_pose.y
                        if np.sqrt((-0.65-x)**2 + (-1.95-y)**2) > max_dist:
                            rospy.loginfo('Frontier pose [' + str(x) + ', '+str(y)+'] is too far from closest frontier.')
                        else:
                            rospy.loginfo('Frontier pose [' + str(x) + ', '+str(y)+'] is close enough.')

                except Exception as e:
                    rospy.loginfo('[EVALUATION] Exception caught: %s.', e)
                    if (c_timer() - t) < timeout:
                        rospy.loginfo('Exception caught during getting frontier. Check script\'s output.')
                    else:
                        rospy.loginfo('Internal timeout exceeded. Get closest frontier service response time is too high.')


                frontier.set_pos(0.6, 1.7)
                rospy.sleep(1)
                rospy.loginfo('Test 4:')
                try:
                    coords = get_frontier()
                    if coords and coords.goal_pose:
                        x = coords.goal_pose.x
                        y = coords.goal_pose.y
                        if np.sqrt((0.45-x)**2 + (1.75-y)**2) > max_dist:
                            rospy.loginfo('Frontier pose [' + str(x) + ', '+str(y)+'] is too far from closest frontier.')
                        else:
                            rospy.loginfo('Frontier pose [' + str(x) + ', '+str(y)+'] is close enough.')

                except Exception as e:
                    rospy.loginfo('[EVALUATION] Exception caught: %s.', e)
                    if (c_timer() - t) < timeout:
                        rospy.loginfo('Exception caught during getting frontier. Check script\'s output.')
                    else:
                        rospy.loginfo('Internal timeout exceeded. Get closest frontier service response time is too high.')


                frontier.set_pos(1.5, - 2.0)
                rospy.sleep(1)
                rospy.loginfo('Test 5:')
                try:
                    coords = get_frontier()
                    if coords and coords.goal_pose:
                        x = coords.goal_pose.x
                        y = coords.goal_pose.y
                        if np.sqrt((1.9-x)**2 + (-1.95-y)**2) > max_dist:
                            rospy.loginfo('Frontier pose [' + str(x) + ', '+str(y)+'] is too far from closest frontier.')
                        else:
                            rospy.loginfo('Frontier pose [' + str(x) + ', '+str(y)+'] is close enough.')

                except Exception as e:
                    rospy.loginfo('[EVALUATION] Exception caught: %s.', e)
                    if (c_timer() - t) < timeout:
                        rospy.loginfo('Exception caught during getting frontier. Check script\'s output.')
                    else:
                        rospy.loginfo('Internal timeout exceeded. Get closest frontier service response time is too high.')

            output = frontier_proc.communicate()

            rospy.loginfo('Frontier script output:')

            if output[0]:
                rospy.loginfo(output[0])
            if output[1]:
                rospy.loginfo(output[1])

        finally:
            frontier_proc.terminate()
            scan_timer.cancel()
            player_proc.kill()
            roscore_proc.kill()

        if not serv_start:
            rospy.loginfo('Timeout while waiting for service to start.')
        
        #frontier.shutdown()
        frontier_proc.terminate()
        player_proc.kill()
        player_proc.kill()
        roscore_proc.kill()
