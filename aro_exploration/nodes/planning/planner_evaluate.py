#!/usr/bin/env python3
from __future__ import absolute_import, division, print_function

# Setup python paths first.
import os
import sys
import rospy
from nav_msgs.msg import OccupancyGrid
from geometry_msgs.msg import TransformStamped
import subprocess
from threading import Timer
import tf.transformations as tft
import tf2_ros
from aro_msgs.srv import PlanPath, PlanPathRequest, PlanPathResponse
from geometry_msgs.msg import Pose2D, Pose, PoseStamped, Point, Quaternion
from scipy.ndimage import morphology

dir = os.path.dirname(os.path.abspath(__file__))

import base64
from glob import glob
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import mimetypes
import numpy as np
from shutil import copytree

np.set_printoptions(suppress=True)

class Grid():
    def __init__(self):
        self.gridSubscriber = rospy.Subscriber('occupancy', OccupancyGrid, self.grid_cb)
        self.tf_pub = tf2_ros.TransformBroadcaster()
        self.x = 0
        self.y = 0
        self.robotDiameter = 0.6
        self.gridReady = False

    def shutdown(self):
        self.gridSubscriber.unregister()

    def grid_cb(self, msg):
        tf_msg = TransformStamped()
        tf_msg.header.stamp = rospy.Time.now()
        tf_msg.transform.translation.x = self.x
        tf_msg.transform.translation.y = self.y
        tf_msg.transform.translation.z = 0
        q = tft.quaternion_from_euler(0, 0, 0)
        tf_msg.transform.rotation.x = q[0]
        tf_msg.transform.rotation.y = q[1]
        tf_msg.transform.rotation.z = q[2]
        tf_msg.transform.rotation.w = q[3]
        tf_msg.header.frame_id = 'icp_map'
        tf_msg.child_frame_id = 'base_footprint'
        self.tf_pub.sendTransform(tf_msg)

        tf_msg = TransformStamped()
        tf_msg.header.stamp = rospy.Time.now()
        tf_msg.header.frame_id = 'map'
        tf_msg.child_frame_id = 'icp_map'
        tf_msg.transform.rotation.w = 1
        self.tf_pub.sendTransform(tf_msg)

    def set_pos(self, x, y):
        self.x = x
        self.y = y

def main():

    total = 0.0
    messages = ''
    output = []
    max_dist = 0.3

    roscore_proc = subprocess.Popen(['roscore'])
    rospy.init_node("tester")
    rospy.set_param('robot_diameter', 0.6)
    rospy.set_param('occupancy_treshold', 90)
    rospy.set_param('map_frame', 'map')
    player_proc = subprocess.Popen(['rosbag', 'play', '../../data/planning/map_dat.bag'], cwd=dir)

    grid = Grid()
    kill = lambda process : process.terminate()

    planner_script_output_file = 'planner_output.txt'
    with open(planner_script_output_file, 'w') as f_output:
        planner_proc = subprocess.Popen(['rosrun', 'aro_exploration', 'planner.py'], stdout=f_output, stderr=f_output)

    rospy.sleep(5)
    timeout = 30
    scan_timer1 = Timer(timeout, kill, [planner_proc])
    scan_timer2 = Timer(timeout, kill, [roscore_proc])
    scan_timer3 = Timer(timeout, kill, [player_proc])
    serv_start = False

    scan_timer1.start()
    scan_timer2.start()
    scan_timer3.start()
        
    serv_start = False
    try:
        rospy.wait_for_service('plan_path', 10)
        plan_path = rospy.ServiceProxy('plan_path', PlanPath)
        serv_start = True
    except Exception as e: 
        planner_proc.terminate()
        rospy.loginfo('Exception caught') 
        return
        
    if serv_start: 

        rospy.sleep(1)
        rospy.loginfo('Test 1:')
        try:
            sx = -0.75
            sy = 1
            gx = 1.1
            gy = -0.8
            req = PlanPathRequest(Pose2D(sx,sy,0),Pose2D(gx,gy,0))
            path = plan_path(req).path

            ref_path = [(68, 19), (67, 20), (66, 21), (65, 22), (64, 23), (63, 24), (62, 25), (61, 26), (60, 26), (59, 27), (58, 27), (57, 27), (56, 28), (55, 29), (54, 30), (53, 31), (52, 32), (51, 33), (50, 34), (49, 35), (48, 36), (47, 37), (46, 38), (45, 38), (44, 39), (43, 40), (42, 41), (41, 42), (40, 43), (39, 44), (38, 45), (37, 46), (36, 47), (35, 48), (34, 49), (33, 50), (33, 51), (33, 52), (33, 53), (33, 54), (33, 55), (32, 56)]
            ref_path_world = [(-0.75, 1.0), (-0.7, 0.95), (-0.65, 0.9), (-0.6, 0.85), (-0.55, 0.8), (-0.5, 0.75), (-0.45, 0.7), (-0.4, 0.65), (-0.4, 0.6), (-0.35, 0.55), (-0.35, 0.5), (-0.35, 0.45), (-0.3, 0.4), (-0.25, 0.35), (-0.2, 0.3), (-0.15, 0.25), (-0.1, 0.2), (-0.05, 0.15), (0.0, 0.1), (0.05, 0.05), (0.1, 0.0), (0.15, -0.05), (0.2, -0.1), (0.2, -0.15), (0.25, -0.2), (0.3, -0.25), (0.35, -0.3), (0.4, -0.35), (0.45, -0.4), (0.5, -0.45), (0.55, -0.5), (0.6, -0.55), (0.65, -0.6), (0.7, -0.65), (0.75, -0.7), (0.8, -0.75), (0.85, -0.75), (0.9, -0.75), (0.95, -0.75), (1.0, -0.75), (1.05, -0.75), (1.1, -0.8)]
            if len(path) > len(ref_path_world):
                rospy.loginfo('Path length suboptimal.')
            # rospy.loginfo('Reference path on grid is:')
            # rospy.loginfo(ref_path)
            path_array = []
            for point in path:
                path_array.append((np.round(point.x,2), np.round(point.y,2)))
            rospy.loginfo('Planner script output path:')
            rospy.loginfo(path_array)

        except Exception as e:
            rospy.loginfo('[EVALUATION] Exception caught: %s.', e)

        rospy.sleep(1)
        messages += '<p> Test 2: '
        try:
            # -0.75,1 to 1.1,-0.8
            sx = 1.1
            sy = -0.8
            gx = 0.9
            gy = 1.8
            req = PlanPathRequest(Pose2D(sx,sy,0),Pose2D(gx,gy,0))
            path = plan_path(req).path
            path_array = []
            for point in path:
                path_array.append((np.round(point.x,2), np.round(point.y,2)))

            rospy.loginfo('Planner script output path:')
            rospy.loginfo(path_array)
            ref_path = [(32, 56), (32, 55), (32, 54), (33, 53), (34, 52), (34, 51), (35, 50), (35, 49), (35, 48), (36, 47), (37, 46), (38, 45), (39, 44), (40, 44), (41, 44), (42, 43), (43, 43), (44, 43), (45, 43), (46, 43), (47, 43), (48, 43), (49, 43), (50, 43), (51, 43), (52, 43), (53, 43), (54, 43), (55, 43), (56, 43), (57, 43), (58, 43), (59, 43), (60, 43), (61, 43), (62, 43), (63, 43), (64, 43), (65, 43), (66, 43), (67, 43), (68, 43), (69, 43), (70, 43), (71, 43), (72, 43), (73, 44), (74, 45), (75, 46), (76, 47), (77, 48), (78, 49), (79, 50), (80, 50), (81, 51), (82, 51), (83, 51), (84, 52)]
            ref_path_world = [(1.1, -0.8), (1.05, -0.8), (1.0, -0.8), (0.95, -0.75), (0.9, -0.7), (0.85, -0.7), (0.8, -0.65), (0.75, -0.65), (0.7, -0.65), (0.65, -0.6), (0.6, -0.55), (0.55, -0.5), (0.5, -0.45), (0.5, -0.4), (0.5, -0.35), (0.45, -0.3), (0.45, -0.25), (0.45, -0.2), (0.45, -0.15), (0.45, -0.1), (0.45, -0.05), (0.45, 0.0), (0.45, 0.05), (0.45, 0.1), (0.45, 0.15), (0.45, 0.2), (0.45, 0.25), (0.45, 0.3), (0.45, 0.35), (0.45, 0.4), (0.45, 0.45), (0.45, 0.5), (0.45, 0.55), (0.45, 0.6), (0.45, 0.65), (0.45, 0.7), (0.45, 0.75), (0.45, 0.8), (0.45, 0.85), (0.45, 0.9), (0.45, 0.95), (0.45, 1.0), (0.45, 1.05), (0.45, 1.1), (0.45, 1.15), (0.45, 1.2), (0.5, 1.25), (0.55, 1.3), (0.6, 1.35), (0.65, 1.4), (0.7, 1.45), (0.75, 1.5), (0.8, 1.55), (0.8, 1.6), (0.85, 1.65), (0.85, 1.7), (0.85, 1.75), (0.9, 1.8)]
            if len(path) > len(ref_path_world):
                rospy.loginfo('Path length suboptimal.')

        except Exception as e:
            rospy.loginfo('[EVALUATION] Exception caught: %s.', e)

        rospy.sleep(1)
        messages += '<p> Test 3: '
        try:
            sx = 0.9
            sy = 1.8
            gx = -1.0
            gy = -0.9
            req = PlanPathRequest(Pose2D(sx,sy,0),Pose2D(gx,gy,0))
            path = plan_path(req).path
            path_array = []
            for point in path:
                path_array.append((np.round(point.x,2), np.round(point.y,2)))

            rospy.loginfo('Planner script output path:')
            rospy.loginfo(path_array)
            ref_path = [(84, 52), (83, 52), (82, 52), (81, 52), (80, 52), (79, 52), (78, 52), (77, 51), (76, 50), (75, 49), (74, 48), (73, 47), (72, 47), (71, 47), (70, 46), (69, 45), (68, 44), (67, 43), (66, 43), (65, 42), (64, 42), (63, 42), (62, 42), (61, 42), (60, 42), (59, 41), (58, 40), (57, 39), (56, 39), (55, 38), (54, 37), (53, 36), (52, 35), (51, 34), (50, 33), (49, 32), (48, 31), (47, 30), (46, 29), (45, 28), (44, 27), (43, 26), (42, 26), (41, 26), (40, 26), (39, 25), (38, 25), (37, 25), (36, 24), (35, 23), (34, 22), (33, 21), (33, 20), (33, 19), (32, 18), (31, 17), (31, 16), (30, 15), (30, 14)]
            ref_path_world = [(0.9, 1.8), (0.9, 1.75), (0.85, 1.7), (0.85, 1.65), (0.8, 1.6), (0.75, 1.55), (0.75, 1.5), (0.7, 1.45), (0.65, 1.4), (0.6, 1.35), (0.6, 1.3), (0.6, 1.25), (0.55, 1.2), (0.5, 1.15), (0.45, 1.1), (0.4, 1.05), (0.4, 1.0), (0.35, 0.95), (0.35, 0.9), (0.35, 0.85), (0.3, 0.8), (0.25, 0.75), (0.2, 0.7), (0.2, 0.65), (0.2, 0.6), (0.2, 0.55), (0.2, 0.5), (0.2, 0.45), (0.15, 0.4), (0.15, 0.35), (0.15, 0.3), (0.1, 0.25), (0.05, 0.2), (0.0, 0.15), (-0.05, 0.1), (-0.1, 0.05), (-0.15, 0.0), (-0.2, -0.05), (-0.25, -0.1), (-0.3, -0.15), (-0.35, -0.2), (-0.4, -0.25), (-0.4, -0.3), (-0.4, -0.35), (-0.4, -0.4), (-0.45, -0.45), (-0.45, -0.5), (-0.45, -0.55), (-0.5, -0.6), (-0.55, -0.65), (-0.6, -0.7), (-0.65, -0.75), (-0.7, -0.75), (-0.75, -0.8), (-0.8, -0.8), (-0.85, -0.85), (-0.9, -0.85), (-0.95, -0.9), (-1.0, -0.9)]

            if len(path) > len(ref_path_world):
                rospy.loginfo('Path length suboptimal.')
        except Exception as e:
            rospy.loginfo('[EVALUATION] Exception caught: %s.', e)

        rospy.sleep(1)
        messages += '<p> Test 4: '
        try:
            sx = 1.1
            sy = -0.8
            gx = -1
            gy = -0.9
            req = PlanPathRequest(Pose2D(sx,sy,0),Pose2D(gx,gy,0))
            path = plan_path(req).path
            path_array = []
            for point in path:
                path_array.append((np.round(point.x,2), np.round(point.y,2)))

            rospy.loginfo('Planner script output path:')
            rospy.loginfo(path_array)
            ref_path = [(32, 56), (32, 55), (32, 54), (32, 53), (32, 52), (32, 51), (32, 50), (32, 49), (32, 48), (32, 47), (32, 46), (32, 45), (32, 44), (32, 43), (32, 42), (32, 41), (32, 40), (32, 39), (32, 38), (32, 37), (32, 36), (32, 35), (32, 34), (32, 33), (32, 32), (32, 31), (32, 30), (32, 29), (32, 28), (32, 27), (32, 26), (32, 25), (32, 24), (32, 23), (32, 22), (32, 21), (32, 20), (32, 19), (31, 18), (31, 17), (31, 16), (30, 15), (30, 14)]
            ref_path_world = [(1.1, -0.8), (1.05, -0.8), (1.0, -0.8), (0.95, -0.8), (0.9, -0.8), (0.85, -0.8), (0.8, -0.8), (0.75, -0.8), (0.7, -0.8), (0.65, -0.8), (0.6, -0.8), (0.55, -0.8), (0.5, -0.8), (0.45, -0.8), (0.4, -0.8), (0.35, -0.8), (0.3, -0.8), (0.25, -0.8), (0.2, -0.8), (0.15, -0.8), (0.1, -0.8), (0.05, -0.8), (0.0, -0.8), (-0.05, -0.8), (-0.1, -0.8), (-0.15, -0.8), (-0.2, -0.8), (-0.25, -0.8), (-0.3, -0.8), (-0.35, -0.8), (-0.4, -0.8), (-0.45, -0.8), (-0.5, -0.8), (-0.55, -0.8), (-0.6, -0.8), (-0.65, -0.8), (-0.7, -0.8), (-0.75, -0.8), (-0.8, -0.85), (-0.85, -0.85), (-0.9, -0.85), (-0.95, -0.9), (-1.0, -0.9)]
            if len(path) > len(ref_path_world):
                rospy.loginfo('Path length suboptimal.')


        except Exception as e:
            rospy.loginfo('[EVALUATION] Exception caught: %s.', e)

        rospy.sleep(1)
        messages += '<p> Test 5: '
        try:
            sx = -0.75
            sy = 1
            gx = 0.9
            gy = 1.8
            req = PlanPathRequest(Pose2D(sx,sy,0),Pose2D(gx,gy,0))
            path = plan_path(req).path
            path_array = []
            for point in path:
                path_array.append((np.round(point.x,2), np.round(point.y,2)))

            rospy.loginfo('Planner script output path:')
            rospy.loginfo(path_array)
            ref_path = [(68, 19), (68, 20), (68, 21), (68, 22), (68, 23), (68, 24), (69, 25), (70, 26), (70, 27), (70, 28), (70, 29), (70, 30), (70, 31), (70, 32), (70, 33), (70, 34), (70, 35), (70, 36), (71, 37), (71, 38), (71, 39), (72, 40), (73, 41), (74, 42), (75, 43), (76, 44), (77, 45), (78, 46), (79, 47), (80, 48), (81, 49), (82, 50), (83, 51), (84, 52)]
            ref_path_world = [(-0.75, 1.0), (-0.7, 1.0), (-0.65, 1.0), (-0.6, 1.0), (-0.55, 1.0), (-0.5, 1.0), (-0.45, 1.05), (-0.4, 1.1), (-0.35, 1.1), (-0.3, 1.1), (-0.25, 1.1), (-0.2, 1.1), (-0.15, 1.1), (-0.1, 1.1), (-0.05, 1.1), (0.0, 1.1), (0.05, 1.1), (0.1, 1.1), (0.15, 1.15), (0.2, 1.15), (0.25, 1.15), (0.3, 1.2), (0.35, 1.25), (0.4, 1.3), (0.45, 1.35), (0.5, 1.4), (0.55, 1.45), (0.6, 1.5), (0.65, 1.55), (0.7, 1.6), (0.75, 1.65), (0.8, 1.7), (0.85, 1.75), (0.9, 1.8)]
            if len(path) > len(ref_path_world):
                rospy.loginfo('Path length suboptimal.')

        except Exception as e:
            rospy.loginfo('[EVALUATION] Exception caught: %s.', e)
        
    planner_proc.terminate()
    rospy.sleep(1)
    grid.shutdown()
    player_proc.kill()
    roscore_proc.kill()
    

if __name__ == '__main__':
    main()
