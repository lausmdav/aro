#!/bin/bash -e

use_config=0
while getopts ":c" option; do
   case $option in
      c) # display Help
         echo "Including config control.yaml in submitted files."
         use_config=1;;
     \?) # Invalid option
         echo "Error: Invalid option"
         exit;;
   esac
done

# Create the Brute submission package for homework 5 without config file

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

source "$dir/utils.bash"

if [[ $use_config -eq 1 ]]; then
  create_hw_package "05_config"
else
  create_hw_package "05"
fi

