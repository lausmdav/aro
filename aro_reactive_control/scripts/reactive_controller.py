#!/usr/bin/env python3
"""
Simple reactive controller for turtlebot robot.
"""

import rospy
import numpy as np  # you probably gonna need this
import math
from geometry_msgs.msg import Twist, Vector3
from aro_msgs.msg import SectorDistances
from std_srvs.srv import SetBool, SetBoolRequest, SetBoolResponse, Trigger

# TODO HW 01 and HW 02: add necessary imports
from sensor_msgs.msg import LaserScan


class ReactiveController():
    Robot_Run = False
    Robot_Start = False

    def __init__(self):
        rospy.loginfo('Initializing node')
        rospy.init_node('reactive_controller')
        self.initialized = False

        # -----------------------------------------------------------------------------------------------------
        # TODO HW 01: register listener for laser scan message
        self.subscriber = rospy.Subscriber('scan', LaserScan, self.scan_cb)

        # -----------------------------------------------------------------------------------------------------
        # TODO HW 02:publisher for "/cmd_vel" topic (use arg "latch=True")
        self.speed_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1, latch=True)
        self.apply_control_inputs(0, 0)

        # -----------------------------------------------------------------------------------------------------
        # TODO HW 01: register publisher for "/reactive_control/sector_dists" topic
        # publishing minimum distances as message of type aro_msgs/SectorDistances
        self.publisher = rospy.Publisher('/reactive_control/sector_dists', SectorDistances, queue_size=1, latch=True)

        # -----------------------------------------------------------------------------------------------------
        # TODO HW 02: create proxy for the mission evaluation service and wait till the start service is up
        rospy.wait_for_service('/reactive_control/evaluate_mission')
        self.evaluation = rospy.ServiceProxy('/reactive_control/evaluate_mission', Trigger)

        # -----------------------------------------------------------------------------------------------------
        # TODO HW 02: create service server for mission start
        self.s = rospy.Service("/reactive_control/activate", SetBool, self.activate_cb)
        rospy.wait_for_service('/reactive_control/activate')
        # -----------------------------------------------------------------------------------------------------
        # TODO: you are probably going to need to add some variables

        # -----------------------------------------------------------------------------------------------------
        # TODO HW 02: add timer for mission end checking

        # -----------------------------------------------------------------------------------------------------

        self.initialized = True
        rospy.loginfo('Reactive controller initialized. Waiting for data.')

    def timer_cb(self, event):
        """
        Callback function for timer.
        :param event (rospy TimerEvent): event handled by the callback
        """
        if not self.initialized:
            return

        # -----------------------------------------------------------------------------------------------------
        # TODO HW 02: Check that the active time had elapsed and send the mission evaluation request
        self.Robot_Start = False
        self.Robot_Run = False
        self.apply_control_inputs(0, 0)
        self.evaluation()

        # -----------------------------------------------------------------------------------------------------

    def activate_cb(self, req: SetBoolRequest) -> SetBoolResponse:
        """
        Activation service callback.
        :param req: obtained ROS service request
        :return: ROS service response
        """

        rospy.loginfo_once('Activation callback entered')

        # -----------------------------------------------------------------------------------------------------
        # TODO HW 02: Implement callback for activation service
        req_back = SetBoolResponse()
        if req.data == True:
            if not self.Robot_Start:
                rospy.Timer(rospy.Duration(49), self.timer_cb, oneshot=True)
            self.Robot_Start = True
            self.Robot_Run = True
        else:
            self.Robot_Start = False
            self.Robot_Run = False
            self.apply_control_inputs(0, 0)

        req_back.success = True
        return req_back

        # -----------------------------------------------------------------------------------------------------

        pass

    def scan_cb(self, scan: LaserScan):
        """
        Scan callback.
        :param msg: obtained message with data from 2D scanner of type ???
        """
        rospy.loginfo_once('Scan callback entered')

        # -----------------------------------------------------------------------------------------------------
        # TODO HW 01: Implement callback for 2D scan, process and publish required data
        def deg_2_idx(angle_deg):
            idx = int(angle_deg / float(np.rad2deg(scan.angle_increment)) + 1)
            # rospy.loginfo(idx)
            return idx

        laser_values = np.array(scan.ranges, dtype=float)
        laser_values[laser_values < float(scan.range_min)] = scan.range_max
        laser_values[laser_values > float(scan.range_max)] = scan.range_max

        # rospy.loginfo('New data')
        laser_values_left = laser_values[deg_2_idx(30):deg_2_idx(90)]
        laser_values_right = laser_values[deg_2_idx(270):deg_2_idx(330)]
        laser_values_front = laser_values[deg_2_idx(330):]
        laser_values_front = np.append(laser_values_front, laser_values[:deg_2_idx(30)])
        laser_values_front_small = laser_values[deg_2_idx(340):]
        laser_values_front_small = np.append(laser_values_front_small, laser_values[:deg_2_idx(20)])
        # rospy.loginfo(laser_values_front)

        msg = SectorDistances()
        msg.distance_left = np.min(laser_values_left)
        msg.distance_front = np.min(laser_values_front)
        msg.distance_right = np.min(laser_values_right)
        distance_front_small = np.min(laser_values_front_small)
        # rospy.loginfo(np.min(laser_values_right))

        self.publisher.publish(msg)

        # -----------------------------------------------------------------------------------------------------
        # TODO HW 02: Add robot control based on received scan
        angular_speed = 0
        left = False
        right = False
        if self.Robot_Run == True:
            if distance_front_small > 0.9:
                left = False
                right = False
                speed = 0.4
                angular_speed = 0
            elif distance_front_small > 0.3:
                speed = 0.4 * msg.distance_front
            else:
                speed = 0

            if msg.distance_front <= 0.8:
                if msg.distance_right > msg.distance_left and not left:
                    right = True
                    angular_speed = -1.3
                if msg.distance_left > msg.distance_right and not right:
                    angular_speed = 1.3
            if distance_front_small > 0.7:
                angular_speed = 0
            self.apply_control_inputs(speed, angular_speed)
        else:
            self.apply_control_inputs(0, 0)
        # -----------------------------------------------------------------------------------------------------

        if not self.initialized:
            return

    def apply_control_inputs(self, velocity: float, angular_rate: float):
        """
        Applies control inputs.
        :param velocity: required forward velocity of the robot
        :param angular_rate: required angular rate of the robot
        """
        # -----------------------------------------------------------------------------------------------------
        # TODO HW 02: publish required control inputs
        linear = Vector3(velocity, 0, 0)  # Speed for linear motion (x, y, z) dType = Float64
        angular = Vector3(0, 0, angular_rate)  # Speed for angular motion (x, y, z) dType = Float64
        self.speed_pub.publish(Twist(linear, angular))
        # -----------------------------------------------------------------------------------------------------

        pass


if __name__ == '__main__':
    rc = ReactiveController()
    rospy.spin()